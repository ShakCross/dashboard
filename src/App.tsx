// import { useState } from "react";
import "./index.css";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faBitcoin } from "@fortawesome/free-brands-svg-icons";
import { Menu } from "./components/Menu";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Activity from "./pages/Activity";
import Deposit from "./pages/Deposit";
import News from "./pages/News";
import Messages from "./pages/Messages";
import Analytics from "./pages/Analytics";
import Announcements from "./pages/Announcements";

function App() {
  return (
    <Router>
      <div className="flex w-full">
        <Menu />
        <Routes>
          <Route path={"/"} element={<Activity />} />
          <Route path="/activity" element={<Activity />} />
          <Route path="/deposit" element={<Deposit />} />
          <Route path="/news" element={<News />} />
          <Route path="/messages" element={<Messages />} />
          <Route path="/analytics" element={<Analytics />} />
          <Route path="/announcements" element={<Announcements />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
