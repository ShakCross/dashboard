import Balance from "../components/Balance";
import Chart from "../components/Chart";
import Nav from "../components/Nav";
import Wallet from "../components/Wallet";

const Activity = () => {
  return (
    <div className="flex flex-col items-center justify-evenly overflow-hidden min-w-70">
      <Nav />
      <Balance />
      <div className="py-12 pl-12 flex justify-between w-full">
        <Chart />
        <Wallet />
      </div>
    </div>
  );
};

export default Activity;
