import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { faker } from "@faker-js/faker";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Title
);

const options = {
  responsive: true,
};

const labels = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
];

const data = {
  labels,
  datasets: [
    {
      data: labels.map(() => faker.datatype.number({ min: 0, max: 60000 })),
      borderColor: "#f8ce36",
      backgroundColor: "#f8ce36",
    },
    {
      data: labels.map(() => faker.datatype.number({ min: 0, max: 60000 })),
      borderColor: "#4b5eaf",
      backgroundColor: "#4b5eaf",
    },
    {
      data: labels.map(() => faker.datatype.number({ min: 0, max: 60000 })),
      borderColor: "#45a6ac",
      backgroundColor: "#45a6ac",
    },
  ],
};

const Chart = () => {
  return (
    <div className="chart-container">
      <Line options={options} data={data} />
    </div>
  );
};

export default Chart;
