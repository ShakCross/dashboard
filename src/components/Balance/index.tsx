import { faBitcoin, faEthereum } from "@fortawesome/free-brands-svg-icons";
import { faLitecoinSign } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Balance = () => {
  return (
    <div className="w-full ml-24 ">
      <div className="text-white flex text-2xl w-full">
        <div>Total Balance</div>
        <div className="ml-5">$183.936</div>
      </div>
      <div className="flex">
        <div className="flex mt-5 bg-yellow-500 rounded-lg p-2 w-40 justify-center text-white text-sm">
          <FontAwesomeIcon className="fa-xl " icon={faBitcoin} />
          <div className="ml-3">1.9678</div>
          <div className="ml-3 flex flex-col items-end">
            <div>BTC</div>
            <div>+12,5%</div>
          </div>
        </div>
        <div className="flex mt-5 bg-blue-500 rounded-lg p-2 w-40 justify-center text-white text-sm ml-6">
          <FontAwesomeIcon className="fa-xl " icon={faEthereum} />
          <div className="ml-3">23.234</div>
          <div className="ml-3 flex flex-col items-end">
            <div>ETH</div>
            <div>-5,23%</div>
          </div>
        </div>
        <div className="flex mt-5 bg-blue-500 rounded-lg p-2 w-40 justify-center text-white text-sm ml-6">
          <FontAwesomeIcon className="fa-xl " icon={faLitecoinSign} />
          <div className="ml-3">380.234</div>
          <div className="ml-3 flex flex-col items-end">
            <div>LTC</div>
            <div>+39,69%</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Balance;
