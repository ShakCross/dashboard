import { faBitcoin, faEthereum } from "@fortawesome/free-brands-svg-icons";
import { faLitecoinSign } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Wallet = () => {
  return (
    <div className="bg-blue-950 m-3 rounded-2xl w-64 h-56 p-5">
      <h1 className="text-white">Wallet Activity</h1>
      <div className="flex justify-between my-4">
        <h6 className="text-white text-xs">History</h6>
        <h6 className="text-gray-400 text-xs">See all</h6>
      </div>
      <div>
        <div className="flex justify-between text-xxs mb-3">
          <div className="flex">
            <FontAwesomeIcon
              className="fa-xl mr-2 text-white"
              icon={faBitcoin}
            />
            <div className="text-blue-600">From BTC Wallet Binance</div>
          </div>
          <div className="flex">
            <div className="text-green-300">+$30.000</div>
            <div className="ml-2 text-blue-600">08/05/2024</div>
          </div>
        </div>
        <div className="flex justify-between text-xxs mb-3">
          <div className="flex">
            <FontAwesomeIcon
              className="fa-xl mr-2  text-white"
              icon={faEthereum}
            />
            <div className="text-blue-600">To Ether Blockchain</div>
          </div>
          <div className="flex">
            <div className="text-green-300">-$5.23</div>
            <div className="ml-2 text-blue-600">07/26/2024</div>
          </div>
        </div>
        <div className="flex justify-between text-xxs mb-3">
          <div className="flex">
            <FontAwesomeIcon
              className="fa-xl mr-2  text-white"
              icon={faLitecoinSign}
            />
            <div className="text-blue-600">To Litecoin Wallet</div>
          </div>
          <div className="flex">
            <div className="text-green-300">+$1.000</div>
            <div className="ml-2 text-blue-600">07/03/2024</div>
          </div>
        </div>
        <div className="flex justify-between text-xxs mb-3">
          <div className="flex">
            <FontAwesomeIcon
              className="fa-xl mr-2 text-white"
              icon={faBitcoin}
            />{" "}
            <div className="text-blue-600">From BTC Wallet</div>
          </div>
          <div className="flex">
            <div className="text-green-300">+$90.0</div>
            <div className="ml-2 text-blue-600">06/10/2024</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Wallet;
