import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

interface OptionProps {
  text: string;
  icon: IconDefinition;
  active?: boolean;
  onClick?: () => void;
}

export const Option = ({ text, icon, active, onClick }: OptionProps) => {
  return (
    <div
      className={`flex rounded-2xl w-72 p-2 my-3 ${
        active ? "bg-blue-600" : "bg-blue-950"
      }`}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`fa-xl ${active ? "text-white" : "text-gray-400"}`}
        icon={icon}
      />
      <h6 className={`ml-5 ${active ? "text-white" : "text-gray-400"} text-sm`}>
        {text}
      </h6>
      {active && (
        <FontAwesomeIcon
          className="fa-xl text-white justify-self-end ml-auto"
          icon={faPlus}
        />
      )}
    </div>
  );
};
