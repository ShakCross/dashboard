import { useState } from "react";
import {
  faArrowRightFromBracket,
  faChartLine,
  faCircleQuestion,
  faEnvelope,
  faFlag,
  faGear,
  faMessage,
  faNewspaper,
  faPeopleRoof,
  faWallet,
} from "@fortawesome/free-solid-svg-icons";
import { Option } from "../Option";
import { faCalendar } from "@fortawesome/free-regular-svg-icons";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const items = [
  {
    text: "Activity",
    icon: faCalendar,
  },
  {
    text: "Deposit",
    icon: faWallet,
  },
  {
    text: "News",
    icon: faNewspaper,
  },
  {
    text: "Messages",
    icon: faEnvelope,
  },
  {
    text: "Analytics",
    icon: faChartLine,
  },
  {
    text: "Announcements",
    icon: faFlag,
  },
];

const support = [
  {
    text: "Settings",
    icon: faGear,
  },
  {
    text: "Help",
    icon: faCircleQuestion,
  },
  {
    text: "Chat",
    icon: faMessage,
  },
];

export const Menu = () => {
  const [activeOption, setActiveOption] = useState(items[0].text);

  return (
    <div className="bg-blue-950 h-full w-96 m-3 rounded-2xl flex flex-col items-center">
      <div className="flex items-center w-72 my-7">
        <FontAwesomeIcon
          className="text-blue-600 fa-2xl bg-slate-900 rounded-full border border-slate-900 p-2"
          icon={faPeopleRoof}
        />
        <h1 className="text-white text-lg ml-4">Meeteam</h1>
      </div>
      <h6 className="w-72 text-gray-400">Dashboard Overview</h6>
      {items.map((item) => (
        <Link to={`/${item.text.toLowerCase()}`}>
          <Option
            text={item.text}
            icon={item.icon}
            active={item.text === activeOption}
            onClick={() => setActiveOption(item.text)}
          />
        </Link>
      ))}
      <div className="w-full grid">
        <hr className="border-gray-400" />
        <h6 className="w-72 text-gray-400 justify-self-center mt-5">Support</h6>
        <div className="flex flex-col items-center">
          {support.map((item) => (
            <Option text={item.text} icon={item.icon} />
          ))}
        </div>
        <hr className="border-gray-400" />
      </div>
      <div className="flex py-8 w-72 items-center justify-between">
        <h6 className="text-white">Meeteam 2024©</h6>
        <FontAwesomeIcon
          className="fa-xl text-white"
          icon={faArrowRightFromBracket}
        />
      </div>
    </div>
  );
};
