import {
  faBell,
  faMagnifyingGlass,
  faTabletScreenButton,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Nav = () => {
  return (
    <div className="pb-12 flex w-full mr-32">
      <div className="flex text-white text-xs w-1/2 justify-end">
        <div className="rounded-xl border border-blue-600 bg-blue-600 px-2 py-0.5">
          Monthly
        </div>
        <div className="rounded-xl border border-gray-700 bg-gray-700 px-2 py-0.5 ml-3">
          Weekly
        </div>
        <div className="rounded-xl border border-gray-700 bg-gray-700 px-2 py-0.5 ml-3">
          Daily
        </div>
      </div>
      <div className="w-1/2 flex  text-gray-500">
        <div className="w-full flex justify-end items-center">
          <FontAwesomeIcon icon={faMagnifyingGlass} />
          <FontAwesomeIcon className="ml-5" icon={faTabletScreenButton} />
          <FontAwesomeIcon
            className="ml-5 text-white rounded-lg border border-blue-600 bg-blue-600 px-1"
            icon={faBell}
          />

          <FontAwesomeIcon className="ml-5" icon={faUser} />
          <h6 className="text-white ml-1">Carlos M.</h6>
        </div>
      </div>
    </div>
  );
};

export default Nav;
